
	returnedMessageFormat = function(error, result, action){
	    if(error){
	        console.log("Error '"+action+"' : "+error);
	        return "Error '"+action;
	    }
	    return "Success '"+action+"' : "+result;
	};

	 messageErrorReturn = function(error, errordDescription, errorUri){
	        var res = {
	            "error": error,
	            "error_description": errordDescription,
	            "error_uri": errorUri
	        };
	        console.log("error call");
	        return res;
	};

	 messageSucessReturn= function(code){
	  return "Success : " + code;
	};

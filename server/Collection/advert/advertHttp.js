/*
  ___       _ _   _       _ _           _   _
 |_ _|_ __ (_) |_(_) __ _| (_)___  __ _| |_(_) ___  _ __
  | || '_ \| | __| |/ _` | | / __|/ _` | __| |/ _ \| '_ \
  | || | | | | |_| | (_| | | \__ \ (_| | |_| | (_) | | | |
 |___|_| |_|_|\__|_|\__,_|_|_|___/\__,_|\__|_|\___/|_| |_|
*/

if (Adverts.find().count() == 0) {
    AdvertsINIT = [{
        title: 'Test 1',
        description: 'description',
        startDate: "2016-10-06T07:48:44+00:00",
        endDate: "2016-10-06T07:48:44+00:00",
        categoryId: "surf",
        status: "new",
        birthDate: "30/01/93",
        price: "50",
        bail: "20",
        imgUrl: "http://imgur.com/r/futurama/Wd9qWP9",
        insurance: "10",
    }, {
        title: 'Test 2',
        description: 'description',
        startDate: "2016-10-06T07:48:44+00:00",
        endDate: "2016-10-06T07:48:44+00:00",
        categoryId: "surf",
        status: "done",
        birthDate: "30/01/93",
        price: "50",
        bail: "20",
        imgUrl: "http://imgur.com/r/futurama/ygm4Y",
        insurance: "10",
    }, {
        title: 'Test 3',
        description: 'description',
        startDate: "2016-10-06T07:48:44+00:00",
        endDate: "2016-10-06T07:48:44+00:00",
        categoryId: "Tennis",
        status: "inProgress",
        birthDate: "30/01/93",
        price: "50",
        bail: "20",
        imgUrl: "http://imgur.com/r/futurama/1kha5se",
        insurance: "10",
    }, {
        title: 'Test 4',
        description: 'description',
        startDate: "2016-10-06T07:48:44+00:00",
        endDate: "2016-10-06T07:48:44+00:00",
        categoryId: "Tennis",
        status: "new",
        birthDate: "30/01/93",
        price: "50",
        bail: "20",
        imgUrl: "http://imgur.com/r/futurama/JlZZRD4",
        insurance: "10",

    }, {
        title: 'Test 5',
        description: 'description',
        startDate: "2016-10-06T07:48:44+00:00",
        endDate: "2016-10-06T07:48:44+00:00",
        categoryId: "Petanque",
        status: "new",
        birthDate: "30/01/93",
        price: "50",
        bail: "20",
        imgUrl: "http://imgur.com/r/futurama/NCT7KSP",
        insurance: "10",
    }]
    _.each(AdvertsINIT, function(_advertsinit) {
        Adverts.insert(_advertsinit);
    });

}




//TODO done filter include
Meteor.method("getadvert", function(userIdParam, limit, offset) {
    try {
        var res = Adverts.find({
            'userId': {
                $ne: userIdParam
            }
        }, {
            skip: offset,
            limit: limit
        }).fetch();
        return res;
    } catch (caughtError) {
        this.setHttpStatusCode(500);
        return messageErrorReturn("Internal server error ", "Oops! Something went wrong...", null);;
    }
}, {
    url: "v1/advert",
    httpMethod: "get",
    getArgsFromRequest: function(request) {
        return [request.userId, request.query.limit, request.query.offset];
    }
});

// TODO done
Meteor.method("getadvertById", function(idParam) {
    try {
        var res = Adverts.findOne({
            "_id": idParam
        }, {
            transform: function(_advert) {
                if (_advert.userRenderIds) {
                    var tpm = []
                    _advert.userRenderIds.forEach(function(_id) {
                        var user = Meteor.users.findOne({
                            "_id": _id
                        });
                        tmps.push(user);
                    });
                    _advert.userRender = tmp;
                }
                return _advert;
            }
        });
        return res;
    } catch (caughtError) {
        this.setHttpStatusCode(500);
        return null;
    }
}, {
    url: "v1/advert/:id",
    httpMethod: "get",
    getArgsFromRequest: function(request) {
        return [request.params.id];

    }
});


//TODO done need userId
Meteor.method("createAdvert", function(userId, advert) {
    try {
        advert["creationDate"] = new Date().toISOString();
        advert["userId"] = userId;
        console.log("here", advert);
        var res = Adverts.insert(advert);
        this.setHttpStatusCode(201);
        return messageSucessReturn("new Advert created");
    } catch (caughtError) {
        this.setHttpStatusCode(500);
        console.log(caughtError);
        return messageErrorReturn("Internal server error ", "Oops! Something went wrong...", null);
    }
}, {
    url: "v1/advert",
    httpMethod: "post",
    getArgsFromRequest: function(request) {
        var content = request.body;
        var jsonObject = {
            "title": content.title,
            "description": content.description,
            "startDate": content.startDate,
            "imgUrl": content.imgUrl,
            "endDate": content.endDate,
            "categoryId": content.categoryId,
            "status": content.status,
            "price": content.price,
            "bail": content.bail,
            "insurance": content.insurance,
        };
        return [request.userId, jsonObject];
    }
});



//TODO done
Meteor.method("updateAdvert", function(idParam, advert) {
    try {
        advert["lastUpdateDate"] = new Date().toISOString();
        var res = Adverts.update({
            _id: idParam
        }, {
            $set: advert
        });
        this.setHttpStatusCode(200);
        return messageSucessReturn("Advert updated");
    } catch (caughtError) {
        this.setHttpStatusCode(500);
        return messageErrorReturn("Internal server error ", "Oops! Something went wrong...", null);
    }
}, {
    url: "v1/advert/:id",
    httpMethod: "put",
    getArgsFromRequest: function(request) {
        var content = request.body;
        var jsonObject = {
            "title": content.title,
            "description": content.description,
            "startDate": content.startDate,
            "endDate": content.endDate,
            "categoryId": content.categoryId,
            "status": content.status,
            "price": content.price,
            "bail": content.bail,
            "insurance": content.insurance,
        };
        return [request.params.id, jsonObject];
    }
});


//TODO done
Meteor.method("deleteAdvert", function(idParam) {
    try {
        var res = Adverts.remove({
            "_id": idParam
        });
        this.setHttpStatusCode(200);
        return messageSucessReturn("Advert " + idParam + " delete");
    } catch (caughtError) {
        this.setHttpStatusCode(500);
        return null;
    }
}, {
    url: "v1/advert/:id",
    httpMethod: "delete",
    getArgsFromRequest: function(request) {
        return [request.params.id];
    }
});

Meteor.method("getFlash", function() {
    try {

        var currentDate = new Date();
        var limitDate = new Date();
        limitDate.setHours(0, 0, 0, 0);
        limitDate.setDate(limitDate.getDate() + 1);
        console.log("yop", currentDate, limitDate);
        var res = Adverts.find({
            "endDate": {
                $gte: currentDate,
                $lt: limitDate
            }
        }).fetch();
        this.setHttpStatusCode(200);
        return res;
    } catch (caughtError) {
        console.log(caughtError);
        this.setHttpStatusCode(500);
        return messageErrorReturn("Internal server error ", "Oops! Something went wrong...", null);
    }
}, {
    url: "v1/advert/flash",
    httpMethod: "get",
    getArgsFromRequest: function(request) {
        return [];
    }
});


Meteor.method("getByUserId", function(userIdParam) {
    try {

        var res = Adverts.find({
            "userId": userIdParam
        }).fetch();
        this.setHttpStatusCode(200);
        return res;
    } catch (caughtError) {
        console.log(caughtError);
        this.setHttpStatusCode(500);
        return messageErrorReturn("Internal server error ", "Oops! Something went wrong...", null);
    }
}, {
    url: "v1/advert/myAnnonce",
    httpMethod: "get",
    getArgsFromRequest: function(request) {
        return [request.userId];
    }
});


Meteor.method("addRenderId", function(userIdParam, advertIdParam) {
    try {
        var res = Adverts.update({
            "_id": "advertIdParam"
        }, {
            userRenderId: {
                $pull: advertIdParam
            }
        });
        this.setHttpStatusCode(200);
        return messageSucessReturn("UserRender added");
    } catch (caughtError) {
        console.log(caughtError);
        this.setHttpStatusCode(500);
        return messageErrorReturn("Internal server error ", "Oops! Something went wrong...", null);
    }
}, {
    url: "v1/advert/:id/addUserRender",
    httpMethod: "post",
    getArgsFromRequest: function(request) {
        return [request.userId, equest.params.id];
    }
});

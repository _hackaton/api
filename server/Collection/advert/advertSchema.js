/*
 __     ___    _     ___ ____    _  _____ ___ ___  _   _ ____ _____ _____ ____  ____   ____ _   _ _____ __  __    _
 \ \   / / \  | |   |_ _|  _ \  / \|_   _|_ _/ _ \| \ | / ___|_   _| ____|  _ \/ ___| / ___| | | | ____|  \/  |  / \
  \ \ / / _ \ | |    | || | | |/ _ \ | |  | | | | |  \| \___ \ | | |  _| | |_) \___ \| |   | |_| |  _| | |\/| | / _ \
   \ V / ___ \| |___ | || |_| / ___ \| |  | | |_| | |\  |___) || | | |___|  __/ ___) | |___|  _  | |___| |  | |/ ___ \
    \_/_/   \_\_____|___|____/_/   \_\_| |___\___/|_| \_|____/ |_| |_____|_|   |____/ \____|_| |_|_____|_|  |_/_/   \_\

 */
var validationStepSchema = new SimpleSchema({
    status: {
        type: Boolean,
        label: "status",
        optional: true,
    },
    updateDate: {
        type: Date,
        label: "updateDate",
        optional: true
    },
})

/*
 __     ___    _     ___ ____    _  _____ ___ ___  _   _ ____   ____ _   _ _____ __  __    _
 \ \   / / \  | |   |_ _|  _ \  / \|_   _|_ _/ _ \| \ | / ___| / ___| | | | ____|  \/  |  / \
  \ \ / / _ \ | |    | || | | |/ _ \ | |  | | | | |  \| \___ \| |   | |_| |  _| | |\/| | / _ \
   \ V / ___ \| |___ | || |_| / ___ \| |  | | |_| | |\  |___) | |___|  _  | |___| |  | |/ ___ \
    \_/_/   \_\_____|___|____/_/   \_\_| |___\___/|_| \_|____/ \____|_| |_|_____|_|  |_/_/   \_\

 */

var validationSchema = new SimpleSchema({
    userRenderId: {
        type: String,
        label: "userRenderId",
        optional: true,
    },
    advertId: {
        type: String,
        label: "advertId",
        optional: true,
    },
    ask: {
        type: validationStepSchema,
        label: "ask",
        optional: true,
    },
    validate: {
        type: validationStepSchema,
        label: "validate",
        optional: true
    },
    inProgress: {
        type: validationStepSchema,
        label: "inProgress",
        optional: true
    },
    done: {
        type: validationStepSchema,
        label: "done",
        optional: true,
    }
});
/*

     _    ______     _______ ____ _____ ____   ____ _   _ _____ __  __    _
    / \  |  _ \ \   / / ____|  _ \_   _/ ___| / ___| | | | ____|  \/  |  / \
   / _ \ | | | \ \ / /|  _| | |_) || | \___ \| |   | |_| |  _| | |\/| | / _ \
  / ___ \| |_| |\ V / | |___|  _ < | |  ___) | |___|  _  | |___| |  | |/ ___ \
 /_/   \_\____/  \_/  |_____|_| \_\|_| |____/ \____|_| |_|_____|_|  |_/_/   \_\


 */



var advertSchema = new SimpleSchema({
    title: {
        type: String,
        label: "title",
        optional: true,
        defaultValue: "",
    },
    description: {
        type: String,
        label: "description",
        optional: true,
        defaultValue: "",
    },
    startDate: {
        type: Date,
        label: "startDate",
        optional: true,
        defaultValue: null,
    },
    endDate: {
        type: Date,
        label: "endDate",
        optional: true,
        defaultValue: null,
    },
    categoryId: {
        type: String,
        label: "categoryId",
        optional: true,
        defaultValue: "",
    },
    status: {
        type: String,
        label: "status",
        optional: true,
        defaultValue: "new",
    },
    price: {
        type: String,
        label: "price",
        optional: true,
        defaultValue: "",
    },
    bail: {
        type: String,
        label: "bail",
        optional: true,
        defaultValue: "",
    },
    userId: {
        type: String,
        label: "userId",
        optional: true,
        defaultValue: "",
    },
    imgUrl: {
        type: String,
        label: "imgUrl",
        optional: true,
        defaultValue: "",
    },
    insurance: {
        type: String,
        label: "insurance",
        optional: true,
        defaultValue: "",
    },
    validation: {
        type: [validationSchema],
        label: "validation",
        optional: true,
        defaultValue: [],
    },
    isFlash: {
        type: Boolean,
        label: "isFlash",
        optional: true,
        defaultValue: false,
    },
    userRenderIds: {
        type: [String],
        label: "userRenderIds",
        optional: true,
        defaultValue: [],
    },
    lastUpdateDate: {
        type: Date,
        label: "lastUpdateDate",
        optional: true,
    },
    creationDate: {
        type: Date,
        label: "creationDate",
        optional: true,
    },
});

Adverts.attachSchema(advertSchema);

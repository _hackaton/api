/*
  ___       _ _   _       _ _           _   _
 |_ _|_ __ (_) |_(_) __ _| (_)___  __ _| |_(_) ___  _ __
  | || '_ \| | __| |/ _` | | / __|/ _` | __| |/ _ \| '_ \
  | || | | | | |_| | (_| | | \__ \ (_| | |_| | (_) | | | |
 |___|_| |_|_|\__|_|\__,_|_|_|___/\__,_|\__|_|\___/|_| |_|
*/


if (Categories.find().count() == 0) {
    CategoryINIT = [{
        "_id": "surf",
        "name": "surf"
    }, {
        "_id": "tennis",
        "name": "tennis",
    }, {
        "_id": "Petanque",
        "name": "Petanque"
    }]
    _.each(CategoryINIT, function(_categoryinit) {
        Categories.insert(_categoryinit);
    });


}

//TODO done need userId
Meteor.method("createCategorie", function(category) {
    try {
        category["creationDate"] = new Date().toISOString();
        console.log(category);
        var res = Categories.insert(category);
        this.setHttpStatusCode(200);
        return messageSucessReturn("Category created");
    } catch (caughtError) {
        this.setHttpStatusCode(500);
        return messageErrorReturn("Internal server error ", "Oops! Something went wrong...", null);
    }
}, {
    url: "v1/category",
    httpMethod: "post",
    getArgsFromRequest: function(request) {
        var content = request.body;
        console.log("name", content.name);
        var jsonObject = {
            "name": content.name
        };
        return [jsonObject];
    }
});


Meteor.method("getCategorie", function(limit, offset) {
    try {
        var res = Categories.find({}, {
            skip: offset,
            limit: limit
        }).fetch();
        this.setHttpStatusCode(200);
        return res;
    } catch (caughtError) {
        this.setHttpStatusCode(500);
        return messageErrorReturn("Internal server error ", "Oops! Something went wrong...", null);
    }
}, {
    url: "v1/category",
    httpMethod: "get",
    getArgsFromRequest: function(request) {
        var content = request.body;
        return [request.query.limit, request.query.offset];
    }
});


Meteor.method("getCategorieById", function(idParam) {
    try {
        var res = Categories.findOne({
            "_id": idParam
        });
        this.setHttpStatusCode(200);
        return res;
    } catch (caughtError) {
        this.setHttpStatusCode(500);
        return messageErrorReturn("Internal server error ", "Oops! Something went wrong...", null);
    }
}, {
    url: "v1/category/:id",
    httpMethod: "get",
    getArgsFromRequest: function(request) {
        var content = request.body;
        return [request.params.id];
    }
});


Meteor.method("removeCategorie", function(idParam) {
    try {
        var res = Categories.remove({
            "_id": idParam
        });
        this.setHttpStatusCode(200);
        return messageSucessReturn("category removed succesfully");
    } catch (caughtError) {
        this.setHttpStatusCode(500);
        return messageErrorReturn("Internal server error ", "Oops! Something went wrong...", null);
    }
}, {
    url: "v1/category/:id",
    httpMethod: "delete",
    getArgsFromRequest: function(request) {
        var content = request.body;
        return [request.params.id];
    }
});


Meteor.method("updateCategorie", function(idParam, category) {
    try {
        category["lastUpdate"] = new Date().toISOString();
        var res = Categories.update({
            "_id": idParam
        }, {
            $set: category
        });
        this.setHttpStatusCode(200);
        return messageSucessReturn("category updated succesfully");
    } catch (caughtError) {
        this.setHttpStatusCode(500);
        return messageErrorReturn("Internal server error ", "Oops! Something went wrong...", null);
    }
}, {
    url: "v1/category/:id",
    httpMethod: "put",
    getArgsFromRequest: function(request) {
        var content = request.body;
        var jsonObject = {
            "name": content.name
        }
        return [request.params.id, jsonObject];
    }
});

/*
   ____    _  _____ _____ ____  ___  ______   ______   ____ _   _ _____ __  __    _
  / ___|  / \|_   _| ____/ ___|/ _ \|  _ \ \ / / ___| / ___| | | | ____|  \/  |  / \
 | |     / _ \ | | |  _|| |  _| | | | |_) \ V /\___ \| |   | |_| |  _| | |\/| | / _ \
 | |___ / ___ \| | | |__| |_| | |_| |  _ < | |  ___) | |___|  _  | |___| |  | |/ ___ \
  \____/_/   \_\_| |_____\____|\___/|_| \_\|_| |____/ \____|_| |_|_____|_|  |_/_/   \_\
 */


var categorySchema = new SimpleSchema({
    name: {
        type: String,
        label: "name",
        optional: true,
        defaultValue: "",
    },
    lastUpdate: {
        type: String,
        label: "lastUpdate",
        optional: true,
        defaultValue: "",
    },
    creationDate: {
        type: String,
        label: "creationDate",
        optional: true,
        defaultValue: "",
    }
});

Categories.attachSchema(categorySchema);

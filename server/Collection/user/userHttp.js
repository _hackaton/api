
/*
  ___       _ _   _       _ _           _   _
 |_ _|_ __ (_) |_(_) __ _| (_)___  __ _| |_(_) ___  _ __
  | || '_ \| | __| |/ _` | | / __|/ _` | __| |/ _ \| '_ \
  | || | | | | |_| | (_| | | \__ \ (_| | |_| | (_) | | | |
 |___|_| |_|_|\__|_|\__,_|_|_|___/\__,_|\__|_|\___/|_| |_|
*/


if (Meteor.users.find().count() == 0) {
    usersINIT = [{
        username: 'kevin',
        email: 'kevin@gmail.com',
        password: 'kevin$api',
        firstname: "Kevin",
        lastname: "Kevin",
        birthDate: "30/01/93",
    }, {
        username: 'fabien',
        email: 'fabien@gmail.com',
        password: 'Hfabien$api',
        firstname: "fabien",
        lastname: "fabien",
        birthDate: "30/01/93",
    }, {
        username: 'meryl',
        email: 'meryl@gmail.com',
        password: 'meryl@gmail.com',
        firstname: "meryl",
        lastname: "Xaysakda",
        birthDate: "30/01/93",
    }, {
        username: 'selom',
        email: 'selom@gmail.com',
        password: 'selom$api',
        firstname: "selom",
        lastname: "selom",
        birthDate: "30/01/93",

    }, {
        username: 'mickael',
        email: 'mickael@gmail.com',
        password: 'mickael$api',
        firstname: "mickael",
        lastname: "mickael",
        birthDate: "30/01/93",
    }]
    _.each(usersINIT, function(_userinit) {
        Accounts.createUser({
            username: _userinit.username,
            password: _userinit.password,
            profile: {
                "firstname": _userinit.firstname,
                "lastname": _userinit.lastname,
                "birthDate": _userinit.birthDate,
                "favoriteId": [],
            }
        });
    });

}




Meteor.method("addUser", function(user) {
    try {
        user.profile["creationDate"] = new Date().toISOString();
        var res = Accounts.createUser({
            username: user.username,
            password: user.password,
            email: user.email,
            profile: user.profile
        });
        this.setHttpStatusCode(200);
        return returnedMessageFormat(null, res, "user added succesfully");
    } catch (caughtError) {
        throw new Meteor.Error(500, returnedMessageFormat(caughtError, null, "oups! something get wrong "));
    }
}, {
    url: "v1/user",
    httpMethod: "post",
    getArgsFromRequest: function(request) {
        var content = request.body;
        var jsonObject = {
            "username": content.username,
            "password": content.password,
            "email": content.email,
            "profile": {
                "firstname": content.firstname,
                "lastname": content.lastname,
                "favoriteId": [],
                "birthDate": content.birthDate,
                "paypalId": content.paypalId,
            }
        };
        return [jsonObject];
    }
});

//done
Meteor.method("getUser", function() {
    try {
        var res = Meteor.users.find({}, {
            transform: function(_res) {
                delete(_res.services);
                return _res;
            }
        }).fetch();
        this.setHttpStatusCode(200);
        return res;
    } catch (caughtError) {
        throw new Meteor.Error(500, returnedMessageFormat(caughtError, null, "oups! something get wrong "));
    }
}, {
    url: "v1/user",
    httpMethod: "get",
    getArgsFromRequest: function(request) {
        return [];
    }
});

//done
Meteor.method("getUserById", function(idParam) {
    try {
        var res = Meteor.users.findOne({
            "_id": idParam
        }, {
            transform: function(_res) {
                delete(_res.services);
                return _res;
            }
        });
        this.setHttpStatusCode(200);
        return res;
    } catch (caughtError) {
        throw new Meteor.Error(500, returnedMessageFormat(caughtError, null, "oups! something get wrong "));
    }
}, {
    url: "v1/user/:id",
    httpMethod: "get",
    getArgsFromRequest: function(request) {
        return [request.params.id];
    }
});



//done
Meteor.method("removeUser", function(idParam) {
    try {
        var res = Meteor.users.remove(idParam)
        this.setHttpStatusCode(200);
        return messageSucessReturn("user removed succesfully");
    } catch (caughtError) {
        throw new Meteor.Error(500, returnedMessageFormat(caughtError, null, "oups! something get wrong "));
    }
}, {
    url: "v1/user/:id",
    httpMethod: "delete",
    getArgsFromRequest: function(request) {
        return [request.params.id];
    }
});


//done
Meteor.method("updateUser", function(idParam, user) {
    try {
        user.profile["lastUpdateDate"] = new Date().toISOString();
        var res = Meteor.users.update(idParam, {
            $set: user
        });
        this.setHttpStatusCode(200);
        return messageSucessReturn("user updated succesfully");
    } catch (caughtError) {
        throw new Meteor.Error(500, returnedMessageFormat(caughtError, null, "oups! something get wrong "));
    }
}, {
    url: "v1/user/:id",
    httpMethod: "put",
    getArgsFromRequest: function(request) {
        var content = request.body;
        var jsonObject = {
            "username": content.username,
            "password": content.password,
            "email": content.email,
            "profile": {
                "firstname": content.firstname,
                "lastname": content.lastname,
                "favoriteId": content.favoriteId,
                "birthDate": content.birthDate,
                "paypalId": content.paypalId,
            }
        };
        return [request.params.id, jsonObject];
    }
});

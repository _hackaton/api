/*
  _   _ ____  _____ ____  ____    ____  ____   ___  _____ ___ _     _____   ____   ____ _   _ _____ __  __    _
 | | | / ___|| ____|  _ \/ ___|  |  _ \|  _ \ / _ \|  ___|_ _| |   | ____| / ___| / ___| | | | ____|  \/  |  / \
 | | | \___ \|  _| | |_) \___ \  | |_) | |_) | | | | |_   | || |   |  _|   \___ \| |   | |_| |  _| | |\/| | / _ \
 | |_| |___) | |___|  _ < ___) | |  __/|  _ <| |_| |  _|  | || |___| |___   ___) | |___|  _  | |___| |  | |/ ___ \
  \___/|____/|_____|_| \_\____/  |_|   |_| \_\\___/|_|   |___|_____|_____| |____/ \____|_| |_|_____|_|  |_/_/   \_\

*/
var usersProfile = new SimpleSchema({
    firstname: {
        type: String,
        label: "firstname",
        optional: true,
        defaultValue: "",
    },
    lastname: {
        type: String,
        label: "lastname",
        optional: true,
        defaultValue: "",
    },

    favoriteId: {
        type: [String],
        label: "favoriteId",
        optional: true,
        defaultValue: "",
    },
    imgUrl: {
        type: String,
        label: "imgUrl",
        optional: true,
        defaultValue: "",
    },
    birthDate: {
        type: String,
        label: "birthDate",
        optional: true,
        defaultValue: "",
    },
    lastUpdateDate: {
        type: String,
        label: "lastUpdateDate",
        optional: true,
        defaultValue: "",
    },
    creationDate: {
        type: String,
        label: "creationDate",
        optional: true,
        defaultValue: "",
    },
    paypalId: {
        type: String,
        label: "paypalId",
        optional: true,
        defaultValue: "",
    },

});


/*
  _   _ ____  _____ ____  ____    ____   ____ _   _ _____ __  __    _
 | | | / ___|| ____|  _ \/ ___|  / ___| / ___| | | | ____|  \/  |  / \
 | | | \___ \|  _| | |_) \___ \  \___ \| |   | |_| |  _| | |\/| | / _ \
 | |_| |___) | |___|  _ < ___) |  ___) | |___|  _  | |___| |  | |/ ___ \
  \___/|____/|_____|_| \_\____/  |____/ \____|_| |_|_____|_|  |_/_/   \_\

*/

var usersSchema = new SimpleSchema({

    username: {
        type: String,
        optional: true,
    },
    emails: {
        type: Array,
        optional: true
    },
    "emails.$": {
        type: Object
    },
    "emails.$.address": {
        type: String,
        regEx: SimpleSchema.RegEx.Email
    },
    "emails.$.verified": {
        type: Boolean
    },
    createdAt: {
        type: Date,
    },
    profile: {
        type: usersProfile,
        optional: true,
    },
    services: {
        type: Object,
        optional: true,
        blackbox: true,
    },
    heartbeat: {
        type: Date,
        optional: true,
    },
});
Meteor.users.attachSchema(usersSchema);

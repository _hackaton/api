RestMiddleware.authentificationMiddleware = function (req, res, next) {
  //console.log(req);
//  console.log(req);
  //console.log(req.headers);
  //console.log("user Id",req.userId);
  if(req.method == "OPTIONS"){
    next();
  }
  else{
    var exit = true;
    if(req.userId)exit = false;

    if(exit){
      var body = {
        error: 'Access denied',
        reason: 'Authentification required',
        details: 'You need to log in before accessing to this url',
        data: ''
      };
      body = JSON.stringify(body, null, 2);

      res.statusCode = 503;
    //  "Cache-Control": "no-store",
  //    "Pragma": "no-cache",
      res.setHeader("Access-Control-Allow-Origin", "*"),
      res.setHeader("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE, OPTIONS"),
      res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
      res.setHeader("Access-Control-Allow-Credentials", true);
      res.setHeader('Content-Type', 'application/json');
      //res.setHeader('Access-Control-Allow-Origin', '*');
      res.write(body);
      res.end();
    }
    else next();
  }

};

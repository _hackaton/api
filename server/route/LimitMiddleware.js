RestMiddleware.limitMiddleware = function(req, res, next){
  var limit = 100;
  var offset = 0;
  if(req.query.offset !== undefined){
    req.query.offset = parseInt(req.query.offset);
  }
  if(req.query.limit !== undefined){
    req.query.limit = parseInt(req.query.limit);
  }
  next();
};

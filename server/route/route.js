JsonRoutes.ErrorMiddleware.use("v1/advert", RestMiddleware.handleErrorAsJson);
JsonRoutes.Middleware.use("v1/advert", JsonRoutes.Middleware.parseBearerToken);
JsonRoutes.Middleware.use("v1/advert", JsonRoutes.Middleware.authenticateMeteorUserByToken);
JsonRoutes.Middleware.use("/v1/advert", RestMiddleware.authentificationMiddleware);
JsonRoutes.Middleware.use("/v1/advert", RestMiddleware.limitMiddleware);



JsonRoutes.ErrorMiddleware.use("v1/category", RestMiddleware.handleErrorAsJson);
JsonRoutes.Middleware.use("v1/category", JsonRoutes.Middleware.parseBearerToken);
JsonRoutes.Middleware.use("v1/category", JsonRoutes.Middleware.authenticateMeteorUserByToken);
JsonRoutes.Middleware.use("/v1/category", RestMiddleware.authentificationMiddleware);
JsonRoutes.Middleware.use("/v1/category", RestMiddleware.limitMiddleware);
